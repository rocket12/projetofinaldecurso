<?php 
require '../modelos/usuario.class.php';
session_start();

    $login = $_POST['login'];
    $senha = md5($_POST['senha']);
        
    $usuario = Usuario::loginUsuario($login, $senha);

    if ($usuario != false){
        $_SESSION['cod'] = $usuario['cod'];
        $_SESSION['nome'] = $usuario['nome'];
        $_SESSION['idade'] = $usuario['idade'];
        $_SESSION['email'] = $usuario['email'];
        $_SESSION['tipo_usuario'] = $usuario['tipo_usuario'];
        header('location:../views/perfil_usuario.php');
    } else {
        unset ($_SESSION['tipo_usuario']);
        unset ($_SESSION['email']);
        unset ($_SESSION['nome']);
        unset ($_SESSION['idade']);
        unset ($_SESSION['cod']);
        header('location:../views/login.php');
    }
?>