<?php

include_once '../banco/database.class.php';
    class Usuario{

        private $nome;
        private $login;
        private $idade;
        private $senha;
        private $email;
        private $tipo_usuario;

        public function cadastroUsuario($nome, $idade, $email, $login, $senha){
            $this->tipo_usuario = 4;
            $this->login = $login;
            $this->senha = $senha;
            $this->idade = $idade;
            $this->email = $email;
            $this->nome = $nome;
            
            $connection = Database::conectarBanco();
            $sql = "INSERT INTO usuarios(nome_usuario, login_usuario, idade_usuario, senha_usuario, email_usuario, tipo_usuario) VALUES ('$this->nome', '$this->login', $this->idade, '$this->senha', '$this->email', $this->tipo_usuario)";

            //print($sql);
            $connection->exec($sql);

            return $connection;
        }
        
        static public function obterUsuario($login, $senha){
            $usuarios = [];
            $connection = Database::conectarBanco();

            $query = $connection->query("SELECT * FROM usuarios WHERE login_usuario = '$login' AND senha_usuario = '$senha'");
            $usuarios = $query->fetchAll(PDO::FETCH_ASSOC);

            return $usuarios;
        }

        static public function loginUsuario($login, $senha){
            $usuarios = Usuario::obterUsuario($login, $senha);
            foreach ($usuarios as $usuario) {		
    
                if ($login == $usuario['login_usuario'] && $senha == $usuario['senha_usuario']){
                    
                    $cod_usuario = $usuario['cod_usuario'];
                    $nome_usuario = $usuario['nome_usuario'];
                    $idade_usuario = $usuario['idade_usuario'];
                    $email_usuario = $usuario['email_usuario'];
                    $tipo_usuario = $usuario['tipo_usuario'];
                    $logarray = [
                        'cod' => "$cod_usuario",
                        'nome' => "$nome_usuario",
                        'idade' => "$idade_usuario",
                        'email' => "$email_usuario",
                        'tipo_usuario' => "$tipo_usuario"];
                    return $logarray; 
                } else {
                    return false;
                }    
            }
        }
    }