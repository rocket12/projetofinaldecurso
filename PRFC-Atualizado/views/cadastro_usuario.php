<!DOCTYPE html>
    <?php

    session_start();

    ?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="../SemanticUI/style.css">
    </head>
        <div class="login">
            <div class="login-screen">    
                <div class="app-title">
                    <h1>Cadastro</h1>
                </div>
                    <body>
                        <div class="container">

                            <form class="form-signin" action="../controlador/cadastro_controlador.php" method="POST">
                                <h2 class="form-signin-heading">Registre-se, Por Favor !</h2>
                                
                                <label for="inputNome" class="sr-only">Nome</label>
                                <input type="text" id="inputNome" class="form-control" name="nome" placeholder="Nome">

                                <label for="inputIdade" class="sr-only">Idade</label>
                                <input type="number" id="inputIdade" class="form-control" name="idade" placeholder="Idade">

                                <label for="inputEmail" class="sr-only">Email</label>
                                <input type="email" id="inputEmail" class="form-control" name="email" placeholder="Email">

                                <label for="inputLogin" class="sr-only">Login</label>
                                <input type="text" id="inputLogin" class="form-control" name="login" placeholder="Crie um login">

                                <label for="inputPassword" class="sr-only">Senha</label>
                                <input type="password" id="inputPassword" class="form-control" name="senha" placeholder="Crie uma senha">
                                
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Cadastrar-me</button>
                            </form>
                        </div>
                    </body>
</html> 