<!DOCTYPE html>
<html>
	<head>
		<title>Pagina para Logar</title>
		<link rel="stylesheet" href="../SemanticUi/semantic.min.css">
	</head>
	<body>
		<div class="login">
			<div class="login-screen">
				<div class="app-title">
					<h1>Login</h1>
				</div>

				<form method="post" action="../controlador/login_controlador.php">
    			<div class="ui segment">
      				<div class="ui inverted form">
        				<div class="two fields">
          				<div class="field">
            		<input type="hidden" name="logar">
            		<label>Login</label>
            		<input placeholder="Login" name="login" type="text">
          				</div>
          		<div class="field">
            		<label>Password</label>
            		<input placeholder="Senha" name="senha" type="password">
          				</div>
        		<button type="submit" class="ui submit button">Logar</button>
      			</div>
    			</div>     
    			</form>    

			</div>	
		</div>
	</body>
</html>