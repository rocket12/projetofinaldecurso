<?php require '../controlador/conteudo_controlador.php'; ?>

<html>

  <head>
     <link rel="stylesheet" type="text/css" href="../SemanticUI/semantic.css">
     <title>titulo da pagina</title>
  </head>

  <body>

    <div class="artig">

      <div class="app-title"><h1>Cadastre um conteúdo confiável</h1></div>

      <form class="ui form" action="?acao=cadastrar" method="POST" enctype="multipart/form-data">

        <!-- CAMPOS APENAS PARA ILUSTRAR O FORMUALRIO -->
        <div class="field">
          <label>Título</label>
          <input type="text" name="tituloConteudo" placeholder="Título do conteúdo">
        </div>
        
        <div class="field">
          <label>Conteúdo</label>
          <input type="text" name="textoConteudo" placeholder="Texto do conteúdo">
        </div>

        <div class="field">
          <label>Midias</label>
          <input type="file" name="midiasConteudo" placeholder="Midia do conteúdo">
        </div>

        <!--  dropdown tipo de Religiões -->
        <div class="field">
          <label>Conteúdo sobre quais dessas Religiões ?</label>

          <div class="ui fluid search selection dropdown">
            <input name="categoriasConteudo" type="hidden">
            <i class="dropdown icon"></i>
            <div class="default text">Opções de Religiões</div>
            <div class="menu">
            
              <?php foreach(carregarCategorias() as $categorias): ?>
                  <div class="item" data-value="<?= $categorias['cod_categoria']?>"><?= $categorias['descricao_categoria']?></div>
              <?php endforeach ?>

            </div>
          </div>
        </div>

        <button class="ui button" type="submit">Postar Conteúdo</button>
      </form>

    </div>

    <!-- .artig -->

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="../SemanticUI/semantic.js"></script>

    <script>
      $('.ui.dropdown').dropdown({allowAdditions: true});
    </script>

  </body>
</html>