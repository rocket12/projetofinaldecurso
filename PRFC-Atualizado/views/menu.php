<?php
session_start();
?>

<div class="ui container">
  <div class="ui large secondary inverted pointing menu">
    <a class="active item" href="index.php">Home</a>
    <a class="item">Categorias</a>
    <div class="right item">

            <?php if(!isset($_SESSION['cod'])){
                echo "<li><a class='ui inverted button' href='cadastro_usuario.php'>Cadastar-se</a></li>";
                echo "<li><a class='ui inverted button' href='login.php'>Login</a></li>";
            }else{
                if($_SESSION['tipo_usuario'] == 1 || $_SESSION['tipo_usuario'] == 3){
                    echo "<li><a class='ui inverted button' href='cadastro_conteudo.php'>Notícia</a></li>";
                    echo "<li><a class='ui inverted button' href=''>Contato</a></li>";
                    echo "<li><a class='ui inverted button' href='../views/perfil_usuario.php'>Perfil</a></li>" ; 
                    echo "<li><a class='ui inverted button' href='../controlador/logout_controlador.php'>Sair</a></li>" ; 
                }else{
                    echo "<li><a class='ui inverted button' href=''>Contato</a></li>";
                    echo "<li><a class='ui inverted button' href='../views/perfil_usuario.php'>Perfil</a></li>" ; 
                    echo "<li><a class='ui inverted button' href='../controlador/logout_controlador.php''>Sair</a></li>" ; 
                }
             }?>
      </div>
      
      <?php
        date_default_timezone_set('America/Sao_Paulo');
          $date = date('Y-m-d H:i');
                echo $date." hrs";
       
        ?>
      </a>
    </div>
  </div>
</div>