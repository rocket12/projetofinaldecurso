-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 04-Ago-2017 às 03:08
-- Versão do servidor: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `site_religioes`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacoes`
--

CREATE TABLE `avaliacoes` (
  `cod_conteudo_av` int(11) NOT NULL,
  `cod_avaliacao` int(11) NOT NULL,
  `cod_usuario_av` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `cod_categoria` int(11) NOT NULL,
  `descricao_categoria` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`cod_categoria`, `descricao_categoria`) VALUES
(1, 'Islamismo'),
(2, 'Catolicismo'),
(3, 'Budismo'),
(4, 'Hinduismo'),
(5, 'Judaismo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comentarios`
--

CREATE TABLE `comentarios` (
  `cod_usuario_coment` int(11) NOT NULL,
  `cod_conteudo_coment` int(11) NOT NULL,
  `cod_comentario` int(11) NOT NULL,
  `data_comentario` date NOT NULL,
  `status_comentario` tinyint(1) DEFAULT NULL,
  `texto_comentario` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `conteudos`
--

CREATE TABLE `conteudos` (
  `cod_conteudo` int(11) NOT NULL,
  `titulo_conteudo` varchar(250) NOT NULL,
  `data_conteudo` date NOT NULL,
  `texto_conteudo` longtext NOT NULL,
  `cod_categoria_cont` int(11) NOT NULL,
  `cod_midia_cont` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `conteudos`
--

INSERT INTO `conteudos` (`cod_conteudo`, `titulo_conteudo`, `data_conteudo`, `texto_conteudo`, `cod_categoria_cont`, `cod_midia_cont`) VALUES
(1, 'Islamismo', '2014-05-07', 'O Islamismo é uma religião monoteísta, ou seja, acredita na existência de um único Deus; é fundamentada nos ensinamentos de Mohammed, ou Muhammad, chamado pelos ocidentais de Maomé. Nascido em Meca, no ano 570, Maomé começou sua pregação aos 40 anos, na região onde atualmente corresponde ao território da Arábia Saudita. Conforme a tradição, o arcanjo Gabriel revelou-lhe a existência de um Deus único.', 1, NULL),
(2, 'Catolicismo', '2014-06-01', 'O catolicismo é uma das mais expressivas vertentes do cristianismo e, ainda hoje, congrega a maior comunidade de cristãos existente no planeta. Segundo algumas estatísticas recentes, cerca de um bilhão de pessoas professam ser adeptas ao catolicismo, que tem o Brasil e o México como os principais redutos de convertidos. De fato, as origens do catolicismo estão ligadas aos primeiros passos dados na história do cristianismo.', 2, NULL),
(3, 'Judaismo', '2014-03-24', 'O judaísmo é considerado a primeira religião monoteísta a aparecer na história. Tem como crença principal a existência de apenas um Deus, o criador de tudo. Para os judeus, Deus fez um acordo com os hebreus, fazendo com que eles se tornassem o povo escolhido e prometendo-lhes a terra prometida.', 5, NULL),
(4, 'Hinduismo', '2013-12-28', 'Principal religião da Índia, o Hinduísmo é um tipo de união de crenças com estilos de vida. Sua cultura religiosa é a união de tradições étnicas. Atualmente é a terceira maior religião do mundo em número de seguidores. Tem origem em aproximadamente 3.000 a.C na antiga cultura Védica.', 4, NULL),
(5, 'Budismo', '2016-11-02', 'O budismo não é só uma religião, mas também um sistema ético e filosófico, originário da região da Índia. Foi criado por Sidarta Gautama (563? - 483 a.C.?), também conhecido como Buda. Este criou o budismo por volta do século VI a.C. Ele é considerado pelos seguidores da religião como sendo um guia espiritual e não um deus. Desta forma, os seguidores podem seguir normalmente outras religiões e não apenas o budismo."', 3, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `mensagens`
--

CREATE TABLE `mensagens` (
  `cod_mensagem` int(11) NOT NULL,
  `data_mensagem` date NOT NULL,
  `titulo_mensagem` varchar(80) NOT NULL,
  `texto_mensagem` varchar(1000) NOT NULL,
  `cod_conteudo_msg` int(11) NOT NULL,
  `cod_usuario_msg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `midias`
--

CREATE TABLE `midias` (
  `cod_midia` int(11) NOT NULL,
  `descricao_midia` varchar(80) NOT NULL,
  `titulo_midia` varchar(80) NOT NULL,
  `caminho_midia` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tipos_usuarios`
--

CREATE TABLE `tipos_usuarios` (
  `descricao_usuario` varchar(20) NOT NULL,
  `cod_tipo_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tipos_usuarios`
--

INSERT INTO `tipos_usuarios` (`descricao_usuario`, `cod_tipo_usuario`) VALUES
('Administrador', 1),
('Moderador', 2),
('Colaborador', 3),
('Comum', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE `usuarios` (
  `cod_usuario` int(11) NOT NULL,
  `nome_usuario` varchar(80) NOT NULL,
  `login_usuario` varchar(80) NOT NULL,
  `idade_usuario` int(3) NOT NULL,
  `senha_usuario` varchar(32) NOT NULL,
  `email_usuario` varchar(80) NOT NULL,
  `tipo_usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`cod_usuario`, `nome_usuario`, `login_usuario`, `idade_usuario`, `senha_usuario`, `email_usuario`, `tipo_usuario`) VALUES
(1, 'Mary Sullivan', 'MarySullivan', 18, '5(41PuNe', 'eget@etiam.com', 1),
(2, 'Orpha Faulkner', 'orphaFaulkner', 30, 'z$13M9Zv', 'est@non.com', 2),
(3, 'Edmund Raymond', 'EdmundRaymond', 25, '005:eJWs', 'enim@aliquet.com', 3),
(4, 'Jadwiga Crane', 'JadwigaCrane', 22, '85$r8QqO', 'nec@dolor.com', 4),
(5, 'Santana Bray', 'SantanaBray', 38, ')154SFxn', 'amet@quisque.com', 1),
(7, 'Sonia da Silva Tetzner', 'sonia', 50, '244ef09657703d584abb887c', 'sonia@gmail.com', 4),
(8, 'Juliana', 'juliana', 25, '4bdbfcf8f733a46607372485', 'juliana@gmail.com', 4),
(9, 'juliano', 'juliano', 17, '698dc19d489c4e4db73e28a713eab07b', 'juliano@gmail.com', 4),
(10, 'teste', 'teste', 10, '698dc19d489c4e4db73e28a713eab07b', 'teste@gmail.com', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `avaliacoes`
--
ALTER TABLE `avaliacoes`
  ADD PRIMARY KEY (`cod_avaliacao`),
  ADD KEY `cod_cont` (`cod_conteudo_av`),
  ADD KEY `fk_avaliacao_1_idx` (`cod_usuario_av`);

--
-- Indexes for table `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`cod_categoria`);

--
-- Indexes for table `comentarios`
--
ALTER TABLE `comentarios`
  ADD PRIMARY KEY (`cod_comentario`),
  ADD KEY `cod_cont` (`cod_conteudo_coment`),
  ADD KEY `fk_comentario_1_idx` (`cod_usuario_coment`);

--
-- Indexes for table `conteudos`
--
ALTER TABLE `conteudos`
  ADD PRIMARY KEY (`cod_conteudo`),
  ADD KEY `cod_tipo_cat` (`cod_categoria_cont`),
  ADD KEY `midia_fk_idx` (`cod_midia_cont`);

--
-- Indexes for table `mensagens`
--
ALTER TABLE `mensagens`
  ADD PRIMARY KEY (`cod_mensagem`),
  ADD KEY `cod_cont` (`cod_conteudo_msg`),
  ADD KEY `fk_mensagem_1_idx` (`cod_usuario_msg`);

--
-- Indexes for table `midias`
--
ALTER TABLE `midias`
  ADD PRIMARY KEY (`cod_midia`);

--
-- Indexes for table `tipos_usuarios`
--
ALTER TABLE `tipos_usuarios`
  ADD PRIMARY KEY (`cod_tipo_usuario`);

--
-- Indexes for table `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`cod_usuario`),
  ADD KEY `cod_tipo_user` (`tipo_usuario`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `avaliacoes`
--
ALTER TABLE `avaliacoes`
  MODIFY `cod_avaliacao` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categorias`
--
ALTER TABLE `categorias`
  MODIFY `cod_categoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `comentarios`
--
ALTER TABLE `comentarios`
  MODIFY `cod_comentario` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `conteudos`
--
ALTER TABLE `conteudos`
  MODIFY `cod_conteudo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `midias`
--
ALTER TABLE `midias`
  MODIFY `cod_midia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tipos_usuarios`
--
ALTER TABLE `tipos_usuarios`
  MODIFY `cod_tipo_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `cod_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `avaliacoes`
--
ALTER TABLE `avaliacoes`
  ADD CONSTRAINT `avaliacoes_ibfk_2` FOREIGN KEY (`cod_conteudo_av`) REFERENCES `conteudos` (`cod_conteudo`),
  ADD CONSTRAINT `fk_avaliacao_1` FOREIGN KEY (`cod_usuario_av`) REFERENCES `usuarios` (`cod_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `comentarios`
--
ALTER TABLE `comentarios`
  ADD CONSTRAINT `comentarios_ibfk_2` FOREIGN KEY (`cod_conteudo_coment`) REFERENCES `conteudos` (`cod_conteudo`),
  ADD CONSTRAINT `fk_comentario_1` FOREIGN KEY (`cod_usuario_coment`) REFERENCES `usuarios` (`cod_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `conteudos`
--
ALTER TABLE `conteudos`
  ADD CONSTRAINT `conteudos_ibfk_1` FOREIGN KEY (`cod_categoria_cont`) REFERENCES `categorias` (`cod_categoria`),
  ADD CONSTRAINT `midia_fk` FOREIGN KEY (`cod_midia_cont`) REFERENCES `midias` (`cod_midia`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `mensagens`
--
ALTER TABLE `mensagens`
  ADD CONSTRAINT `fk_mensagem_1` FOREIGN KEY (`cod_usuario_msg`) REFERENCES `usuarios` (`cod_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `mensagens_ibfk_1` FOREIGN KEY (`cod_conteudo_msg`) REFERENCES `conteudos` (`cod_conteudo`);

--
-- Limitadores para a tabela `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`tipo_usuario`) REFERENCES `tipos_usuarios` (`cod_tipo_usuario`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
