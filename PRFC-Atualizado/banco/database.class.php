<?php

class Database{

    private static $usuario    = "root";
    private static $senha      = "root";
    private static $nome_banco = "site_religioes";

	public static function conectarBanco() {

		try {
            
            $conexao = new PDO("mysql:host=localhost;dbname=".self::$nome_banco, self::$usuario, self::$senha); 
            $conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conexao;

    	} catch(PDOException $erro){
            echo "Erro de Conexao: ". $erro->getMessage();
    	}
    }
}